import EhExcel from './src/main';

EhExcel.install = function(Vue) {
    Vue.component(EhExcel.name, EhExcel);
};

export default EhExcel;