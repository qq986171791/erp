import EhExcel from './packages/EhExcel';
import Vue from 'vue';

const components = [
    EhExcel
];

const install = function(Vue,opts = {}) {
    components.map(component => {
        Vue.component(component.name, component);
    });
}

if (Vue) {
    install(Vue);
}

export default {
    EhExcel
};


