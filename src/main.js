// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import "./service/interceptor";
import store from '@/service/store';
import router from './service/router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/scss/base.scss'
import ErhuUI from '@/erhu-ui';


Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(ElementUI)
Vue.use(ErhuUI)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
