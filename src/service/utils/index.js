import store from "@/service/store";
import Vue from 'vue'
let i  = 0;
let utils = {
    decoratebeforeFn(target, key, descriptor){
    	console.log(descriptor); //test
		return descriptor;
	},
	hasAuth(){
		console.log(store.getters);
	},
	rmLocalStore : name => {
		if (!name) return;
		window.localStorage.removeItem(name);
	},
	getLocalStore : name => {
		if (!name) return;
		return window.localStorage.getItem(name);
	},
	setLocalStore : (name, content) => {
		if (!name) return;
		if (typeof content !== 'string') {
			content = JSON.stringify(content);
		}
		window.localStorage.setItem(name, content);
	},
	isArray : (o) => {
		return Object.prototype.toString.call(o) == '[object Array]';
	},
	isObject : (o) => {
		return Object.prototype.toString.call(o) == '[object Object]';
	},
	isString : (o) => {
		return Object.prototype.toString.call(o) == '[object String]';
	},
	isUndefined : (o) => {
		return Object.prototype.toString.call(o) == '[object Undefined]';
	},
	isNull : (o) => {
		return Object.prototype.toString.call(o) == '[object Null]';
	},
	isEmpty : (o) => {
		return o == 'undefined' || utils.isUndefined() || utils.isNull() || (utils.isString(o) && !o.length);
	},
	each : (o,callback) => {
		if(utils.isArray(o)){
			for(var i = 0; i < o.length; i++){
				callback(o[i],i);
			}
		}
		if(utils.isObject(o)){
			for(var key in o){
				callback(o[key],key);
			}
		}
	},
	//交换元素
	swapInArray(arr,a,b,isEl){
		if(isEl){
			a = arr.indexOf(a)
			b = arr.indexOf(b)
			if(a == -1 || b == -1){
				return;
			}
		}
		let t = null;
		t = arr[a];
        Vue.set(arr, a, arr[b])
        Vue.set(arr, b, t)
	},
	//交换字段
    swapFieldInArray(tree,node1,node2,field){
        if(!node1 || !node2){
            return;
        }
        let id1 = node1.id,
            id2 = node2.id;
        if(id1 == id2){
            return;
        }
        utils.eachTree(tree,(item) => {
            if(item.children && item.children.length > 0){
                let i1 = item.children.findIndex((item) => {
                        return item.id == id1;
                    }),
                    i2 = item.children.findIndex((item) => {
                        return item.id == id2;
                    });
                if(i1 != -1 && i2 != -1){
                	let a = item.children[i1],
						b = item.children[i2];
                    let t = null;
                    // t = a[field];
                    // a[field] = b[field];
                    // b[field] = t;
					t = item.children[i1].sort;
                    item.children[i1].sort = item.children[i2].sort;
                    item.children[i2].sort = t;
                }
            }
        })
		return tree;
    },
	rmTreeNode(tree,name){
		this.eachTree(tree,(node) => {
			this.each(node.children,(item,i) => {
				if(item.name == name){
					node.children.splice(i,1);
				}
				if(node.children && node.children.length <= 0){
					this.rmTreeNode(tree,node.name);
				}
			});
		})
	},
	eachTree:(node,callback) => {
		if(node.name == 'tyroot'){
			callback(node);
		}
        if(node.children && node.children.length > 0){
            utils.each(node.children,(item,index) => {
            	callback(item,node.children,index);
                utils.eachTree(item,callback)
            });
        }
	},
    getType(obj){
        //tostring会返回对应不同的标签的构造函数
        let toString = Object.prototype.toString;
        let map = {
            '[object Boolean]'  : 'boolean',
            '[object Number]'   : 'number',
            '[object String]'   : 'string',
            '[object Function]' : 'function',
            '[object Array]'    : 'array',
            '[object Date]'     : 'date',
            '[object RegExp]'   : 'regExp',
            '[object Undefined]': 'undefined',
            '[object Null]'     : 'null',
            '[object Object]'   : 'object'
        };
        if(obj instanceof Element) {
            return 'element';
        }
        return map[toString.call(obj)];
    },
	extend(p,c){
        for (let i in p) {
            if (typeof p[i] === 'object') {
                c[i] = (p[i].constructor === Array) ? [] : {};
                this.extend(p[i], c[i]); // 递归调用
            } else {
                c[i] = p[i];
            }
        }
        return c;
	},
	sortByKeys(keys,o){
		let ret = {};
		this.each(keys,(k,i) => {
			ret[k] = o[k];
		})
        return ret;
	},
	sortChildrenForTree(tree){
		this.eachTree(tree,(node) => {
			if(node.children && node.children.length > 0){
				node.children.sort((a,b) => {
					return a.sort - b.sort;
				});
			}
		})
		return tree;
	},
	log(...args){
        if(process.env.NODE_ENV == 'development'){
            console.log("%c调试信息开始","background: rgba(252,234,187,1);background: -moz-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%,rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: -webkit-gradient(left top, right top, color-stop(0%, rgba(252,234,187,1)), color-stop(12%, rgba(175,250,77,1)), color-stop(28%, rgba(0,247,49,1)), color-stop(39%, rgba(0,210,247,1)), color-stop(51%, rgba(0,189,247,1)), color-stop(64%, rgba(133,108,217,1)), color-stop(78%, rgba(177,0,247,1)), color-stop(87%, rgba(247,0,189,1)), color-stop(100%, rgba(245,22,52,1)));background: -webkit-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: -o-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: -ms-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: linear-gradient(to right, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fceabb', endColorstr='#f51634', GradientType=1 );")
            console.log(...args);
            console.log("%c调试信息结束","background: rgba(252,234,187,1);background: -moz-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%,rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: -webkit-gradient(left top, right top, color-stop(0%, rgba(252,234,187,1)), color-stop(12%, rgba(175,250,77,1)), color-stop(28%, rgba(0,247,49,1)), color-stop(39%, rgba(0,210,247,1)), color-stop(51%, rgba(0,189,247,1)), color-stop(64%, rgba(133,108,217,1)), color-stop(78%, rgba(177,0,247,1)), color-stop(87%, rgba(247,0,189,1)), color-stop(100%, rgba(245,22,52,1)));background: -webkit-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: -o-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: -ms-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: linear-gradient(to right, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fceabb', endColorstr='#f51634', GradientType=1 );")
        }
	}
}

export default utils;