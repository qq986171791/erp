import utils from '@/service/utils';
export default {
    subscribe(store) {
        this.set(store);
        // utils.log('持久化数据','更新','新数据为',this.get());
        store.subscribe(() => {
            this.set(store);
            // utils.log('持久化数据','更新','新数据为',this.get());
        })
    },
    // 将状态记录到 localStorage
    set(store){
        let data = store.state
        if (data.lock) {
            // 当状态为锁定, 不记录
            return
        }
        data = JSON.stringify(data)
        data = encodeURIComponent(data)
        if (window.btoa) {
            data = btoa(data)
        }
        window.localStorage.setItem('erpDontRemoveMe', data)
    },
    get(){
        let data = window.localStorage.getItem('erpDontRemoveMe');
        if(window.atob){
            data = atob(data);
        }
        data = decodeURIComponent(data);
        data = JSON.parse(data);
        return data;
    }
}
