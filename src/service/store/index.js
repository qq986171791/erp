import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './action'
import getters from './getters'
import {state} from './state';
import local from '@/service/local';
import PageBasedataHome from './modules/PageBasedataHome';
import PageTableDetail from './modules/PageTableDetail';

Vue.use(Vuex)
// 即便模块化，mutations里面也不要有重名函数!!!
let store = new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
    modules:{
        PageBasedataHome,
        PageTableDetail
    }
});
local.subscribe(store);

export default store;

