
export default {
    getState(state){
        //返回最新的状态
        return state;
    },
	token(state){
		return state.token;
	},
	config(state){
		return state.config;
	},
	dev(state){
		return state.dev;
	},
    pluginMain(state){
        return state.pluginMain;
    },
    pluginDetail(state){
        return state.pluginDetail;
    },
    pluginSideNav(state){
		return state.pluginSideNav;
	},
	pluginSideNavTree(state){
    	return state.pluginSideNavTree;
	}
}