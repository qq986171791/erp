import immutable from 'immutable'

export const state = {
    count:0,
    dev:true,
    token:null,
    config:{
        baseUrl:"",
        routerMode:"hash",
        imgBaseUrl:""
    },
    pluginDetail:{
        menu:{
            auth:{'create':true,'edit':true}
        },
        print:{
            auth:{'print':true,'action':true,'export':true},
            custom:{
                show:false
            }
        },
        pagination:{
            pageSize:20,
            pageNumber:1,
            currentPage:1,
            pageSizes:[20,40,60,80,100],
            total:400
        },
        menuBar:{
            auth:{'cert':true}
        },
        status:{
            on:0
        }
    },
    pluginSideNav:{

    },
    pluginSideNavTree:{
        on:{
            label:""
        }
    }
}