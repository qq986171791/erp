import utils from "@/service/utils"
import * as Types from './mutation-types.js'

export default {
    [Types.saveState]({commit},payload){
        commit(Types.saveState,payload);
    }
}