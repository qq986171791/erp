import immutable from 'immutable';
import utils from '@/service/utils';
import _ from 'lodash';
export default {
    saveState (state, payload) {
        let ret = _.merge(state,payload);
        return ret;
    },
    setState(state, current){
        state.current = current;
    }
}
