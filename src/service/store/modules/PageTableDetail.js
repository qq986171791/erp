/**
 * Created by saturn on 2017/5/8.
 */

import * as Types from '../mutation-types';
import * as Api from '../axios/api';
import immutable from 'immutable';
import utils from '@/service/utils';

export default {
    state: immutable.fromJS({
        bb:2
    }),
    getters: {},
    actions: {
        getList({commit}){
            Api.getList().then(res => {
                commit(Types.getList, res.data);
            });
        },
        addCount({commit}){
            commit(Types.addCount,1);
        }
    },
    mutations: {
        [Types.getList](state, payload){
            console.log('mutation -> add');
            state.count += payload;
        },
        [Types.addCount](state, payload){
            console.log(payload);
            state.count += payload;
        }
    }
};
