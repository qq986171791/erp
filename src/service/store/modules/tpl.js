/**
 * Created by saturn on 2017/5/8.
 */

import * as Types from '../mutation-types';
import * as Api from '../axios/api';

export default {
    state: {
        count: 1,
        list: []
    },
    getters: {},
    actions: {
        getList({commit}){
            Api.getList().then(res => {
                commit(Types.getList, res.data);
            });
        }
    },
    mutations: {
        [Types.getList](state, payload){
            console.log('mutation -> add');
            state.count += payload;
        }
    }
};;
