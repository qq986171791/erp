/**
 * Created by saturn on 2017/5/8.
 */

import * as Types from '../mutation-types';
import * as Api from '../axios/api';
import immutable from 'immutable';
import utils from '@/service/utils';

export default {
    state:{
        pluginMain:{
            table:{
                selected:[],
                lists:[],
                tree:{
                    name:'tyroot',
                    prop:"",
                    children:[
                        {
                            id:1,
                            pid:0,
                            type:'index',
                            width:"50",
                            sort:0,
                            visible:true
                        },
                        {
                            id:2,
                            pid:0,
                            type:'selection',
                            width:"55",
                            sort:1,
                            visible:true
                        },
                        {
                            id:3,
                            pid:0,
                            name:'Date',
                            prop:"date",
                            data:[
                                '2016-05-0111111111113',
                                '2016-05-02'
                            ],
                            children:[],
                            sort:2,
                            visible:true
                        },
                        {
                            id:4,
                            pid:0,
                            name:'Delivery Info',
                            prop:"",
                            visible:true,
                            children:[
                                {
                                    id:6,
                                    pid:4,
                                    name:'Address Info',
                                    prop:"",
                                    visible:true,
                                    children:[
                                        {
                                            id:7,
                                            pid:6,
                                            name:'State',
                                            prop:"state",
                                            visible:true,
                                            data:[
                                                'California',
                                                'California33333333333333'
                                            ],
                                            children:[],
                                            sort:0
                                        },
                                        {
                                            id:18,
                                            pid:6,
                                            name:'City',
                                            prop:"city",
                                            visible:true,
                                            data:[
                                                'Los Angeles',
                                                'Los Angeles'
                                            ],
                                            children:[],
                                            sort:1
                                        },
                                        {
                                            id:8,
                                            pid:6,
                                            name:'Address',
                                            prop:"address",
                                            visible:true,
                                            data:[
                                                'No. 189, Grove St, Los Angeles',
                                                'No. 189, Grove St, Los Angeles22222'
                                            ],
                                            children:[],
                                            sort:2
                                        },
                                        {
                                            id:9,
                                            pid:6,
                                            name:'Zip',
                                            prop:"zip",
                                            visible:true,
                                            data:[
                                                'CA 90036',
                                                'CA 90036333333'
                                            ],
                                            children:[],
                                            sort:3
                                        }
                                    ],
                                    sort:0
                                }
                            ],
                            sort:3
                        },
                    ]
                }
            },
            menu:{
                auth:{'create':true,'import':true,'autosize':true}
            },
            pagination:{
                pageSize:20,
                pageNumber:1,
                currentPage:1,
                pageSizes:[20,40,60,80,100],
                total:400
            },
            print:{
                auth:{'print':true,'action':true,'export':true},
                custom:{
                    show:false,
                    tree:{
                        node:{
                            dragNode:null
                        }
                    }
                }
            }
        },
    },
    getters: {},
    actions: {
        getList({commit}){
            Api.getList().then(res => {
                commit(Types.getList, res.data);
            });
        },
        addCount({commit}){
            commit(Types.addCount,1);
        }
    },
    mutations: {
        [Types.getList](state, payload){
            console.log('mutation -> add');
            state.count += payload;
        },
        [Types.addCount](state, payload){
            console.log(payload);
            state.count += payload;
        }
    }
};
