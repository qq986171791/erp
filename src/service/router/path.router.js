import PageUserLogin from '@/components/PageUserLogin'
import PageMessageHome from '@/components/PageMessageHome'
import PageBasedataHome from '@/components/PageBasedataHome'
import PageSkillHome from '@/components/PageSkillHome'

import PageSaleHome from '@/components/PageSaleHome'
import PageBuyHome from '@/components/PageBuyHome'
import PageQaHome from '@/components/PageQaHome'
import PageWarehouseHome from '@/components/PageWarehouseHome'

import PageMakeHome from '@/components/PageMakeHome'
import PageAccountHome from '@/components/PageAccountHome'
import PageQueryHome from '@/components/PageQueryHome'
import PageCountHome from '@/components/PageCountHome'

import PageTableDetail from '@/components/PageTableDetail'

export default [
	{
		path: '',
		redirect: '/page-user-login'
	},
    {
        path: '/page-table-detail',
        name: 'PageTableDetail',
        component: PageTableDetail
    },
	{
		path: '/page-user-login',
		name: 'PageUserLogin',
		component: PageUserLogin
	},
    {
        path: '/page-message-home',
        name: 'PageMessageHome',
        component: PageMessageHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-basedata-home',
        name: 'PageBasedataHome',
        component: PageBasedataHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-skill-home',
        name: 'PageSkillHome',
        component: PageSkillHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-sale-home',
        name: 'PageSaleHome',
        component: PageSaleHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-buy-home',
        name: 'PageBuyHome',
        component: PageBuyHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-qa-home',
        name: 'PageQaHome',
        component: PageQaHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-warehouse-home',
        name: 'PageWarehouseHome',
        component: PageWarehouseHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-make-home',
        name: 'PageMakeHome',
        component: PageMakeHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-account-home',
        name: 'PageAccountHome',
        component: PageAccountHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-query-home',
        name: 'PageQueryHome',
        component: PageQueryHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
    {
        path: '/page-count-home',
        name: 'PageCountHome',
        component: PageCountHome,
        meta: {
            requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        }
    },
]