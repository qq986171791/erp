import Vue from 'vue'
import Router from 'vue-router'
import pathRouter from "./path.router"
import {interceptorRouter} from "./interceptor.router"
import store from '@/service/store'

Vue.use(Router)

let router = new Router({
    linkActiveClass:"on",
    linkExactActiveClass:"on",
	routes:pathRouter,
	mode:store.getters.getState.config.routerMode,
	strict: process.env.NODE_ENV !== 'production',
	scrollBehavior (to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition
		} else {
			return { x: 0, y: 0 }
		}
	}
});

interceptorRouter(router)

export default router;
