import store from '@/service/store'
export function interceptorRouter(o) {
	o.beforeEach((to, from, next) => {
        if (!store.getters.getState.dev && to.meta.requireAuth) {  // 判断该路由是否需要登录权限
            if (store.getters.getState.token) {  // 通过vuex state获取当前的token是否存在
				next();
			}
			else {
				next({
					path: '/page-user-login',
					query: {redirect: to.fullPath}  // 将跳转的路由path作为参数，登录成功后跳转到该路由
				})
			}
		}
		else {
            next();
        }
	})
}