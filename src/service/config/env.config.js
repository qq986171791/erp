/**
 * 配置编译环境和线上环境之间的切换
 * 
 * baseUrl: 域名地址
 * routerMode: 路由模式
 * imgBaseUrl: 图片所在域名地址
 * 
 */

import store from "@/service/store";

if(process.env.NODE_ENV == 'development'){
    store.dispatch('saveDev',true)
    store.dispatch('saveConfig',{
        imgBaseUrl : 'http://testDomin/img/'
    })
}

if(process.env.NODE_ENV == 'production'){
    store.dispatch('saveDev',false)
    store.dispatch('saveConfig',{
        baseUrl : 'http://domin',
        imgBaseUrl : 'http://domin/img/'
    })
}

